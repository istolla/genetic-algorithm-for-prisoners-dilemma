package tactics;

import java.util.BitSet;
import java.util.List;

import game.Payoff;

/**
 * Created by Istolla on 5/30/2017.
 */
public class AllC extends Strategy {
    public AllC() {
        super();
        setDNA(generateDNA());
    }

    public AllC(String name) {
        super(name);
        setDNA(generateDNA());
    }

    private BitSet generateDNA() {
        BitSet dna = new BitSet();
        for (int i = 0; i < getDNALength(); i++)
            dna.set(i);
        return dna;
    }
}
