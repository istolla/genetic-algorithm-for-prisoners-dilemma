package tactics;

import java.util.BitSet;
import java.util.List;

import game.Payoff;

/**
 * Created by Istolla on 5/30/2017.
 */
public class AllD extends Strategy {
    public AllD() {
        super();
        setDNA(new BitSet());
    }

    public AllD(String name) {
        super(name);
        setDNA(new BitSet());
    }
}
