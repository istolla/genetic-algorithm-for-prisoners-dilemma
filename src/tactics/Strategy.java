package tactics;

import java.util.BitSet;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import game.Payoff;
import game.Rules;
import population1.Genetics;

/**
 * Created by Istolla on 5/28/2017.
 */
public class Strategy {
    private static final int PRECURSOR_LENGTH = 6;
    private Map<BitSet, Integer> moveTable = new HashMap<>();
    private String name;
    private int dnaLength;
    private int iteration = 0;
    private BitSet dna = new BitSet(); //Sequence determines strategy prisoner strategy.

    public Strategy() {
        setName(getClass().getSimpleName());
        setDNALength(Genetics.getDNALength());
        generateTable();
        generateDNA();
    }

    public Strategy(String name) {
        this();
        setName(name);
        generateDNA();
    }

    public Strategy(BitSet dna) {
        this();
        generateTable();
        setDNA(dna);
    }

    public Strategy(String name, BitSet dna) {
        this(dna);
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BitSet getDna() {
        return dna;
    }

    public void setDNA(BitSet dna) {
        this.dna = dna;
    }

    /**
     * The strategy of this base class is to always cooperate.
     *
     * @param pastPayoffs The history of previous payoffs.
     * @return True to cooperate or false to defect.
     */
    public boolean getMove(List<Payoff> pastPayoffs) {
        boolean move = true;
        if (iteration == 0)
            move = dna.get(iteration);
        else if (iteration == 1) {
            if (pastPayoffs.get(0).equals(Payoff.R) || pastPayoffs.get(0).equals(Payoff.T))
                move = dna.get(1); //Move for when opponent cooperates on their first move.
            else
                move = !dna.get(1); //Move for when opponent defects on their first move
        } else if (iteration == 2) {
            /*Determine response to opponent's second move*/
            if (pastPayoffs.get(1).equals(Payoff.R))
                move = dna.get(2); //Move for when opponent  CC
            if (pastPayoffs.get(1).equals(Payoff.T))
                move = dna.get(3); //Move for when opponent  CD
            if (pastPayoffs.get(1).equals(Payoff.S))
                move = dna.get(4); //Move for when opponent  DC
            if (pastPayoffs.get(1).equals(Payoff.P))
                move = dna.get(5); //Move for when opponent  DD
        } else {
            /*Response for next move based on opponent's last 3 moves.*/
            BitSet history = new BitSet();
            for (int i = 0; i < 3; i++) {
                if (pastPayoffs.get(i).equals(Payoff.R)) {
                    history.set(i * 2);
                    history.set((i * 2) + 1);
                }
                if (pastPayoffs.get(i).equals(Payoff.T)) {
                    /*Opponent did CD.*/
                    history.set(i * 2);
                    history.clear((i * 2) + 1);
                }
                if (pastPayoffs.get(i).equals(Payoff.S)) {
                    history.clear(i * 2);
                    history.set((i * 2) + 1);
                }
                if (pastPayoffs.get(i).equals(Payoff.P)) {
                    history.clear(i * 2);
                    history.clear((i * 2) + 1);
                }
            }
            /*Get index of move based on opponent's move history.*/
            int index = getTableIndex(history);
            move = dna.get(index);
            history.clear();
        }
        iteration++;
        return move;
    }

    public void resetIteration(){
        iteration = 0;
    }

    public int getDNALength() {
        return dnaLength;
    }

    @Override
    public String toString() {
        String result = new String();
        for (int i = 0; i < Genetics.PRECURSOR_LENGTH; i++) {
            if (dna.get(i))
                result += 'C';
            else
                result += 'D';
        }
        result += '|';
        for (int i = Genetics.PRECURSOR_LENGTH; i < dnaLength; i++) {
            if (dna.get(i))
                result += 'C';
            else
                result += 'D';
        }
        return result;
    }

    private void setDNALength(int dnaLength) {
        this.dnaLength = dnaLength;
    }

    private void generateDNA() {
        Random random = new Random(Calendar.getInstance().getTimeInMillis());
        for (int i = 0; i < getDNALength(); i++) {
            double r = random.nextDouble();
            if (r > 0.5)
                dna.set(i);
        }
    }

    private void generateTable() {
        String s;
        BitSet history;
        for (int i = 0; i < Rules.NUMBER_OF_GAMES; i++) {
            s = getMoveBinary(i);
            history = new BitSet(PRECURSOR_LENGTH);
            for (int j = 0; j < PRECURSOR_LENGTH; j++)
                if (s.charAt(j) == '0')
                    history.set(j);
            moveTable.put(history, i);
        }
    }

    private String getMoveBinary(int number) {
        String result = Integer.toBinaryString(number);
        while (result.length() < PRECURSOR_LENGTH)
            result = '0' + result;
        return result;
    }

    private int getTableIndex(BitSet history) {
        return moveTable.get(history);
    }
}


