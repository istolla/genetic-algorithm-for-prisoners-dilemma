package tactics;

import java.util.BitSet;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import game.Payoff;
import population1.Genetics;

/**
 * Created by Istolla on 5/30/2017.
 */
public class TFT extends Strategy {
    public TFT() {
        super();
        setDNA(generateDNA());
    }

    public TFT(String name) {
        super(name);
        setDNA(generateDNA());
    }

    public BitSet generateDNA() {
        Random random = new Random(Calendar.getInstance().getTimeInMillis());
        BitSet dna = new BitSet();

        /*Generate precursor string*/
        /*Determine whether this TFT strategy cooperates or defects as first move.*/
        if (random.nextDouble() > 0.5)
            dna.set(0);
        for (int i = 1; i < 4; i++)
            dna.set(i);
        for (int i = Genetics.PRECURSOR_LENGTH; i < Genetics.getDNALength(); i++)
            if (random.nextDouble() > 0.5)
                dna.set(i);
        return dna;
    }
}
