package game;

/**
 * Created by Istolla on 5/28/2017.
 */
public class Round {
    private Prisoner p1, p2;

    public Round(Prisoner p1, Prisoner p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public void play() {
        boolean p1Move, p2Move;
        for (int i = 0; i < Rules.NUMBER_OF_GAMES; i++) {
            p1Move = p1.move();
            p2Move = p2.move();
            if (p1Move && p2Move) {
                p1.addScore(Payoff.R);
                p2.addScore(Payoff.R);
            } else if (p1Move && !p2Move) {
                p1.addScore(Payoff.S);
                p2.addScore(Payoff.T);
            } else if (!p1Move && p2Move) {
                p1.addScore(Payoff.T);
                p2.addScore(Payoff.S);
            } else {
                p1.addScore(Payoff.P);
                p2.addScore(Payoff.P);
            }
        }
        p1.reset();
        p2.reset();
    }
}
