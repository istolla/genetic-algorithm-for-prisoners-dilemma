package game;

import java.util.List;
import java.util.stream.Collectors;

import population1.Genetics;

/**
 * Created by Istolla on 5/29/2017.
 */
public class Cycle {
    List<Prisoner> prisoners;

    public Cycle(List<Prisoner> prisoners) {
        this.prisoners = prisoners;
    }

    public void simulate() {
        Round round;
        prisoners.forEach(Prisoner::resetScore);
        for (int i = 0; i < prisoners.size() - 1; i++) {
            for (int j = i + 1; j < prisoners.size(); j++) {
                round = new Round(prisoners.get(i), prisoners.get(j));
                round.play();
            }
        }
    }
}
