package game;

/**
 * Created by Istolla on 5/28/2017.
 */
public class Rules {
    public static final int NUMBER_OF_GAMES = 64; //The number of games played in 1 round

    public static int getScore(Payoff payoff) {
        int score = -1;
        switch (payoff) {
            case R:
                score = 3;
                break;
            case T:
                score = 5;
                break;
            case S:
                score = 0;
                break;
            case P:
                score = 1;
                break;
            default:
                //TODO throw an exception
                break;
        }
        return score;
    }

    public static int getMaxPopulationScore(int numberOfPlayers) {
        int numberOfOpponents = numberOfPlayers - 1;
        return getScore(Payoff.R) * NUMBER_OF_GAMES * numberOfOpponents * numberOfPlayers;
    }

    public static int getMaxIndividualScore(int numberOfPlayers) {
        return getScore(Payoff.T) * NUMBER_OF_GAMES * numberOfPlayers;
    }
}
