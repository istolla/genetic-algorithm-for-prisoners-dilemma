package game;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import tactics.Strategy;

/**
 * Created by Istolla on 5/28/2017.
 */
public class Prisoner {
    private String name;
    private Strategy strategy;
    private int totalScore = 0;
    private List<Payoff> payoffs = new ArrayList<>(); //Store payoffs
    private double fitness = 0;

    public Prisoner(Strategy strategy) {
        setName(strategy.getName() + "_" + getClass().getName());
        setStrategy(strategy);
    }

    public Prisoner(String name, Strategy strategy) {
        this(strategy);
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public void addScore(Payoff payoff) {
        /*Keep track of the last 3 opponent moves.*/
        if(payoffs.size() == 3)
            payoffs.remove(0);
        totalScore += Rules.getScore(payoff);
        payoffs.add(payoff);
    }

    public int getScore() {
        return totalScore;
    }

    public boolean move() {
        return strategy.getMove(payoffs);
    }

    public void resetScore() {
        totalScore = 0;
    }

    public void reset(){
        payoffs.clear();
        strategy.resetIteration();
    }

    public BitSet getGeneticCode() {
        return strategy.getDna();
    }

    public int getDnaLength() {
        return strategy.getDNALength();
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public void printGeneticCode() {
        System.out.println(strategy);
    }
}
