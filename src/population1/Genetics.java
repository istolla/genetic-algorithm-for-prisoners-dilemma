package population1;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Random;

import game.Cycle;
import game.Prisoner;
import game.Rules;
import tactics.Strategy;

/**
 * Created by Istolla on 5/29/2017.
 */
public class Genetics {
    public static final int PRECURSOR_LENGTH = 6;
    public static final int POPULATION_SIZE = 20;
    private static final double CROSSOVER_PROBABILITY = 0.8;
    private static final double MUTATION_PROBABILITY = 0.01;
    private static final double NORMALIZED_MINIMUM = .05; //5% Chance of selection
    private static final double NORMALIZED_MAXIMUM = .20; //20% Chance of selection
    private List<Prisoner> prisoners;
    private Result result;
    private int generation = 0;
    private int lastParentIndex = -1;
    private Random random;

    public Genetics(List<Prisoner> prisoners) {
        this.prisoners = prisoners;
        random = new Random(Calendar.getInstance().getTimeInMillis());
    }

    public static int getDNALength() {
        return Rules.NUMBER_OF_GAMES + PRECURSOR_LENGTH;
    }

    public void simulateEvolution(int numberOfGenerations) {
        if (numberOfGenerations > 0) {
            for (int i = 0; i < numberOfGenerations; i++)
                nextGeneration(); //Produce next generation
        } else
            simulateCycle();
    }

    public void setFitness() {
        prisoners.forEach(p -> p.setFitness(
                (double) p.getScore() / Rules.getMaxIndividualScore(prisoners.size())));
        normalizeFitness();
    }

    public Result getResult() {
        return result;
    }

    private void normalizeFitness() {
        DoubleSummaryStatistics statistics = prisoners.stream().mapToDouble(x -> x.getFitness())
                                                      .summaryStatistics();
        double max = (double) statistics.getMax();
        double min = (double) statistics.getMin();
        if (max != min)
            prisoners.forEach(prisoner -> prisoner.setFitness(NORMALIZED_MINIMUM + (
                    ((prisoner.getFitness() - min) * (NORMALIZED_MAXIMUM - NORMALIZED_MINIMUM)) / (
                            max - min))));
        else
            prisoners.forEach(prisoner -> prisoner.setFitness(NORMALIZED_MAXIMUM));
    }

    private void simulateCycle() {
        Cycle cycle = new Cycle(prisoners);
        cycle.simulate();
        setFitness();
        result = new Result(prisoners);
    }

    private void nextGeneration() {
        if (generation == 0)
            simulateCycle();
        List<Prisoner> newGeneration = new ArrayList<>();
        while (newGeneration.size() < POPULATION_SIZE) {
            Prisoner p1, p2;
            p1 = prisoners.get(getParentIndex());
            p2 = prisoners.get(getParentIndex());
            List<Prisoner> children = breed(p1, p2);
            if (newGeneration.size() + 1 == POPULATION_SIZE)
                newGeneration.add(children.get(0)); //Case where there is an odd population
            else
                newGeneration.addAll(children);
        }
        prisoners.clear();
        prisoners.addAll(newGeneration);
        simulateCycle();
        generation++;
    }

    private List<Prisoner> breed(Prisoner p1, Prisoner p2) {
        List<Prisoner> children = new ArrayList<>();
        BitSet dna1 = p1.getGeneticCode(), dna2 = p2.getGeneticCode();
        int crossoverPoint = random.nextInt(getDNALength());
        if (random.nextDouble() < CROSSOVER_PROBABILITY) {
            BitSet c1 = new BitSet();
            c1.or(dna1);
            c1.clear(crossoverPoint, getDNALength());
            BitSet c2 = new BitSet();
            c2.or(dna1);
            c2.clear(0, crossoverPoint);
            BitSet c3 = new BitSet();
            c3.or(dna2);
            c3.clear(crossoverPoint, getDNALength());
            BitSet c4 = new BitSet();
            c4.or(dna2);
            c4.clear(0, crossoverPoint);

            /*Recombine*/
            dna1 = new BitSet();
            dna2 = new BitSet();
            dna1.or(c1);
            dna1.or(c4);
            dna2.or(c2);
            dna2.or(c3);

            /*Mutate*/
            mutate(dna1);
            mutate(dna2);

            /*Create children*/
            children.add(new Prisoner(new Strategy(dna1)));
            children.add(new Prisoner(new Strategy(dna2)));
        } else {
            children.add(new Prisoner(new Strategy(dna1)));
            children.add(new Prisoner(new Strategy(dna2)));
        }
        children.forEach(p -> p.setName("Generation_" + generation + "_" + p.getName()));
        return children;
    }

    private int getParentIndex() {
        int parentIndex = 0;
        double totalPrisonerFitness = prisoners.stream()
                                               .mapToDouble(prisoner -> prisoner.getFitness())
                                               .sum();
        double target = totalPrisonerFitness * random.nextDouble();
        /*Sort by prisoners by fitness*/
        prisoners.sort(Comparator.comparing(Prisoner::getFitness).reversed());
        double sum = 0;
        while (sum < target) {
            if (parentIndex != lastParentIndex)
                sum += prisoners.get(parentIndex).getFitness();
            parentIndex++;
            parentIndex %= Genetics.POPULATION_SIZE;
        }
        lastParentIndex = parentIndex;
        return parentIndex;
    }

    private void mutate(BitSet dna) {
        for (int i = 0; i < Rules.NUMBER_OF_GAMES; i++)
            if (random.nextDouble() < MUTATION_PROBABILITY)
                dna.flip(i);
    }
}
