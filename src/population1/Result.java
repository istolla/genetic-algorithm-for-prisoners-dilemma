package population1;

import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.List;

import game.Prisoner;
import game.Rules;

/**
 * Created by Istolla on 5/30/2017.
 */
public class Result {
    private List<Prisoner> prisoners;

    public Result(List<Prisoner> prisoners) {
        this.prisoners = prisoners;
    }

    public List<Prisoner> getPrisoners() {
        return prisoners;
    }

    public int getPopulationTotalScore() {
        return prisoners.stream().mapToInt(prisoner -> prisoner.getScore()).sum();
    }

    public double getAverageFitness() {
        DoubleSummaryStatistics stats = prisoners.stream().mapToDouble(
                p -> p.getScore() / (double) Rules.getMaxIndividualScore(Genetics.POPULATION_SIZE))
                                                 .summaryStatistics();
        return stats.getAverage();
    }

    public double getPopulationFitness() {
        return (double) getPopulationTotalScore() / Rules.getMaxPopulationScore(prisoners.size());
    }

    public Prisoner getFittest() {
        return prisoners.stream().max(Comparator.comparingInt(Prisoner::getScore)).get();
    }

    public Prisoner getLeastFittest() {
        return prisoners.stream().min(Comparator.comparingInt(Prisoner::getScore)).get();
    }

    @Override
    public String toString() {
        String result = new String();
        String r0 = "Population Size: " + prisoners.size();
        String r1 = "Total Population Score: " + getPopulationTotalScore();
        String r2 = "Population Fitness: " + getPopulationFitness();
        String r3 = "Best individual fitness: " + Double.toString(
                getFittest().getScore() / (double) Rules
                        .getMaxIndividualScore(Genetics.POPULATION_SIZE));
        String r4 = "Worst individual fitness: " + Double.toString(
                getLeastFittest().getScore() / (double) Rules
                        .getMaxIndividualScore(Genetics.POPULATION_SIZE));
        String r5 = "Average Fitness: " + getAverageFitness();
        String r6 = "Best individual score: " + getFittest().getScore();
        String r7 = "Worst individual score: " + getLeastFittest().getScore();
        String r8 = "Fittest Strategy: " + getFittest().getStrategy();
        String r9 = "Most Unfit Strategy: " + getLeastFittest().getStrategy();
        result += r0 + "\n";
        result += r1 + "\n";
        result += r2 + "\n";
        result += r3 + "\n";
        result += r4 + "\n";
        result += r5 + "\n";
        result += r6 + "\n";
        result += r7 + "\n";
        result += r8 + "\n";
        result += r9 + "\n";
        return result;
    }
}
