import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import game.Prisoner;
import population1.Genetics;
import tactics.AllC;
import tactics.AllD;
import tactics.Strategy;
import tactics.TFT;

/**
 * Created by Istolla on 5/28/2017.
 */
public class Main {
    public static void main(String... args) {
        int numberOfGenerations = 100000;
        Genetics population = new Genetics(allC_TFT());
        population.simulateEvolution(numberOfGenerations);
        System.out.println("allC & TFT");
        System.out.println("Number of generations: " + numberOfGenerations);
        System.out.println(population.getResult());
    }

    public static List<Prisoner> allD_TFT() {
        List<Prisoner> prisoners = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < Genetics.POPULATION_SIZE; i++) {
            double r = random.nextDouble();
            if (r < 0.2)
                prisoners.add(new Prisoner(new Strategy())); //Noise
            else if (r < 0.6)
                prisoners.add(getAllD());
            else
                prisoners.add(getTFT());
        }
        return prisoners;
    }

    public static List<Prisoner> allC_TFT() {
        List<Prisoner> prisoners = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < Genetics.POPULATION_SIZE; i++) {
            double r = random.nextDouble();
            if (r < 0.2)
                prisoners.add(new Prisoner(new Strategy())); //Noise
            else if (r < 0.6)
                prisoners.add(getAllC());
            else
                prisoners.add(getTFT());
        }
        return prisoners;
    }

    public static List<Prisoner> random_TFT() {
        List<Prisoner> prisoners = new ArrayList<>();
        Random random = new Random(Calendar.getInstance().getTimeInMillis());
        for (int i = 0; i < Genetics.POPULATION_SIZE; i++) {
            if (random.nextDouble() < 0.5)
                prisoners.add(new Prisoner(new Strategy())); //Noise
            else
                prisoners.add(getTFT());
        }
        return prisoners;
    }

    public static Prisoner getAllC() {
        return new Prisoner(new AllC());
    }

    public static Prisoner getAllD() {
        return new Prisoner(new AllD());
    }

    public static Prisoner getTFT() {
        return new Prisoner(new TFT());
    }
}
